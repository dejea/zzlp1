package com.zzty.zzlp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.zzty.zzlp.dao")
@SpringBootApplication
@ServletComponentScan
@EnableScheduling
public class ZzlpApplication extends SpringBootServletInitializer {
	public ZzlpApplication()
	{
		super();
		setRegisterErrorPageFilter(false);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ZzlpApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ZzlpApplication.class,args);
	}
}
