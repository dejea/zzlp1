package com.zzty.zzlp.service;

import com.zzty.zzlp.entity.Vtmassage;

import java.util.List;

public interface VtmassageService {

    public Boolean addVtmassages(Vtmassage vtmassage);
    public List<Vtmassage> searchVtmassages();

    void deleteCus();
}
