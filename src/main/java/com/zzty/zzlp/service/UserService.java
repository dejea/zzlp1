package com.zzty.zzlp.service;

import com.zzty.zzlp.entity.User;

public interface UserService {

    public User searchUser(User user);
}
