package com.zzty.zzlp.util;

import com.zzty.zzlp.commom.PropertyUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CreatePicByWord {
    /*public static void main(String[] args){
        createImage("999");
    }*/

    // 根据str,font的样式以及输出文件目录
    public static String createImage(String name){
        // 创建图片
        String path = PropertyUtil.prop.getProperty("path.img")+name+"ava.png";
        /*String path = "C://temp//avatar//"+name+"ava.png";*/
        Font font = new Font("宋体", Font.BOLD, 80);
        File outFile = new File(path);
        Integer width = 100;
        Integer height = 100;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();
        g.setClip(0, 0, width, height);
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.white);
        g.setFont(font);// 设置画笔字体
        /** 用于获得垂直居中y */
        Rectangle clip = g.getClipBounds();
        FontMetrics fm = g.getFontMetrics(font);
        int ascent = fm.getAscent();
        int descent = fm.getDescent();
        int y = (clip.height - (ascent + descent)) / 2 + ascent;
        for (int i = 0; i < 6; i++) {// 256 340 0 680
            g.drawString(name.substring(0,1), 10, y);// 画出字符串
        }
        g.dispose();
        try {
            ImageIO.write(image, "png", outFile);// 输出png图片
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

}
