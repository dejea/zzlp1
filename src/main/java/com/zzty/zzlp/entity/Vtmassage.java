package com.zzty.zzlp.entity;

import java.util.Date;

public class Vtmassage {
    private int    id;
    private String realName;
    private String phoneNumber;
    private String massage;
    private String origin;
    private String title;
    private String   massageTime;
    private String userIp;
    private String equipment;
    private String statusId;
    private String desire;
    private String ipcity;





    public String getDesire() {
        return desire;
    }

    public void setDesire(String desire) {
        this.desire = desire;
    }



    public String getIpcity() {
        return ipcity;
    }

    public void setIpcity(String ipcity) {
        this.ipcity = ipcity;
    }


    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMassageTime() {
        return massageTime;
    }

    public void setMassageTime(String massageTime) {
        this.massageTime = massageTime;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Vtmassage{" +
                "id=" + id +
                ", realName='" + realName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", massage='" + massage + '\'' +
                ", origin='" + origin + '\'' +
                ", title='" + title + '\'' +
                ", massageTime=" + massageTime +
                ", userIp='" + userIp + '\'' +
                ", equipment='" + equipment + '\'' +
                ", statusId='" + statusId + '\'' +
                ", desire='" + desire + '\'' +
                ", ipcity='" + ipcity + '\'' +
                '}';
    }
}
