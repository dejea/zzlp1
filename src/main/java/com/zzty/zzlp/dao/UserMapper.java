package com.zzty.zzlp.dao;

import com.zzty.zzlp.entity.User;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {

    @Select("Select * From user where userName = #{userName} and password = #{password}" )
    public User searchUser(User user);



}
