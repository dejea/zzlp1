package com.zzty.zzlp.dao;

import com.zzty.zzlp.entity.Vtmassage;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface VtmassageMapper {

    @Insert("insert into vtmassage (realName,phoneNumber,massage,origin,title,massageTime,userIp,equipment,statusId,desire,ipcity ) values (#{realName},#{phoneNumber},#{massage},#{origin},#{title},#{massageTime},#{userIp},#{equipment},#{statusId},#{desire},#{ipcity})")
    public Integer addVtmassages(Vtmassage vtmassage);


    @Select("Select * From vtmassage where desire ='1'  ORDER BY `id` DESC" )
    public List<Vtmassage> searchVtmassages();



    @Delete("delete from vtmassage where 1=1")
    int deleteCus();
}
