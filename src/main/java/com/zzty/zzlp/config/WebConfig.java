package com.zzty.zzlp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 进行资源镜像处理
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		System.out.println("----------静态资源配置--------");
		registry.addResourceHandler("/static/**").addResourceLocations("file:/");
		super.addResourceHandlers(registry);
	}
	
}
