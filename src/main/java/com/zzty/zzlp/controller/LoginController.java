package com.zzty.zzlp.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zzty.zzlp.commom.ExcelException;
import com.zzty.zzlp.commom.ExcelUtil;
import com.zzty.zzlp.commom.PropertyUtil;
import com.zzty.zzlp.commom.TimeGet;
import com.zzty.zzlp.entity.User;
import com.zzty.zzlp.entity.Vtmassage;
import com.zzty.zzlp.service.UserService;
import com.zzty.zzlp.service.VtmassageService;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class LoginController {


    @Autowired
    VtmassageService vtmassageService;
    @Autowired
    UserService userService;
    @RequestMapping("/login")
    public void loginPage(HttpServletResponse response, HttpServletRequest request){
        System.out.println( request.getRequestURI());
        try {
            response.sendRedirect("/zzlp/html/login.html");
            System.out.println("------------------------------------------------------------------------");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @PostMapping( "/login/AddVtmassage")
    @ResponseBody
    public RequestEntity addUser(@RequestBody Vtmassage dto) throws Exception {
//        Vtmassage vtmassage = new Vtmassage();
        System.out.println("----------------------------------------------------------------");
//        dto.setTitle(toUTF8(dto.getTitle()));
        String b = dto.getTitle();
//        b = new String(b.getBytes("ISO-8859-1"), "UTF-8");
//        dto.setTitle(b);
        dto.setMassageTime(TimeGet.nowTimeSteing());
        vtmassageService.addVtmassages(dto);
        URI u = null;
        try {
             u = new URI("/login/AddVtmassage");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return new RequestEntity(HttpMethod.POST,u);
//        return true;
    }
    @PostMapping( "/login/judge")
    @ResponseBody
    public boolean judjeLogin(HttpServletRequest request,HttpServletResponse response,@RequestBody User user) throws IOException {
        HttpSession session = request.getSession();
        User dbUser = userService.searchUser(user);
//        System.out.println(dbUser.toString());
        if (dbUser!=null){
            session.setAttribute("user",dbUser);
            System.out.println(true);
            return true;
        }else{
            System.out.println(false);
            return false;
        }
        }
        @PostMapping( "/login/searchlist")
        @ResponseBody
        public List<Vtmassage> searchList(HttpServletRequest request, HttpServletResponse response, @RequestBody User user) throws IOException {
            HttpSession session = request.getSession();
            Object object =  session.getAttribute("user");
//            Object object = 1 ;
            if (object!=null){
                List<Vtmassage> listV= vtmassageService.searchVtmassages();
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                System.out.println(listV.toString());
                System.out.println("===================================================================================================================");
                return listV;
            }
            return null;
        }




        @PostMapping( "/login/download")
        @ResponseBody
        public  JSONArray downloadEXL(HttpServletRequest request, HttpServletResponse response) throws Exception {
            HttpSession session = request.getSession();
//            Object object =  1;
            Object object =  session.getAttribute("user");
            if (object!=null){
//                FileOutputStream fos = null;
//                File file = new File( PropertyUtil.prop.getProperty("path.tmp")+new Date().getTime()+".xls");
                List<Vtmassage> listV= vtmassageService.searchVtmassages();
//                String title = "留言后台数据";
//                String[] rowsName = getRowName(listV.get(0));
//                LinkedHashMap<String,String> fieldMap=new LinkedHashMap<String, String>();
//                    for (int j = 0; j < rowsName.length; j++){
//                        fieldMap.put(rowsName[j], rowsName[j]);
//                    }
////                }
//                if (!file.exists()) {
//                    file.createNewFile();
//                }
//                fos = new FileOutputStream(file);
//                ExcelUtil.listToExcel(listV,fieldMap,"后台数据表",listV.size()+1,fos);
//                try {
//                    if (fos != null) {
//                        fos.close();
//                    }
//                } catch (IOException ex) {
//                }
//                String path = file.getPath();
//                System.out.println("path"+path);
//                File file1 = new File("path");
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                JSONArray jsonArray =new JSONArray();


                for (int i = 0; i < listV.size(); i++) {
                    Vtmassage vtmassage = listV.get(i);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id",         vtmassage.getId()         );
                    jsonObject.put("realName",vtmassage.getRealName()   );
                    jsonObject.put("phoneNumber",vtmassage.getPhoneNumber());
                    jsonObject.put("massage",vtmassage.getMassage()    );
                    jsonObject.put("origin",vtmassage.getOrigin()     );
                    jsonObject.put("title",vtmassage.getTitle()      );
                    jsonObject.put("massageTime",vtmassage.getMassageTime());
                    jsonObject.put("userIp",vtmassage.getUserIp()     );
                    jsonObject.put("equipment",vtmassage.getEquipment()  );
                    jsonObject.put("statusId",vtmassage.getStatusId()   );
                    jsonObject.put("desire",vtmassage.getDesire()     );
                    jsonObject.put("ipcity",vtmassage.getIpcity()         );
//                  System.out.println(listV.get(i));
                    jsonArray.add(jsonObject);
                }
                JSONObject jb = new JSONObject();
                jb.put("id",         "id");
                jb.put("realName",   "姓名");
                jb.put("phoneNumber","手机号");
                jb.put("massage",    "信息");
                jb.put("origin",     "来源");
                jb.put("title",      "标题");
                jb.put("massageTime","时间");
                jb.put("userIp",     "IP地址");
                jb.put("equipment",  "设备");
                jb.put("statusId",   "statusId");
                jb.put("desire",     "意愿");
                jb.put("ipcity",     "地址");
                jsonArray.add(jb);
                return jsonArray;
//                return listV;
            }
            return null;
        }


//        public String[] downloadEXL(List<Vtmassage> vtmassages){
//            Vtmassage vt = vtmassages.get(0);
//            return null;
//        }

    public static String[] getRowName(Vtmassage vtmassage) {
        try {
            PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
            PropertyDescriptor[] descriptors = propertyUtilsBean.getPropertyDescriptors(vtmassage);
            String str[] = new String[descriptors.length-1];
            for (int i = 0; i < descriptors.length; i++) {
                String name = descriptors[i].getName();

                if (!"class".equals(name)) {
                    str[i] = name;
//                    System.out.println(name+":"+ propertyUtilsBean.getNestedProperty(vtmassage, name));
                }
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
//
//    public static  <T>  void   lis2tToExcel (List<T> list , LinkedHashMap<String,String> fieldMap, String sheetName, int sheetSize, HttpServletResponse response
//    ) throws ExcelException{
//
//    }


//    public static Double getFieldValueByFieldName(String fieldName,Object object) {
//        try {
//            Field field = object.getClass().getDeclaredField(fieldName);
//            //对private的属性的访问
//            field.setAccessible(true);
//            return (Double) field.get(object);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

}
