package com.zzty.zzlp.commom;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public class FileUpload {

    public static Map<String, String> uploadFile(MultipartFile fi, String flag) throws Exception {
        Map<String, String> mapReq = new HashMap<String, String>();
        Date date = new Date();
        String fileName = fi.getOriginalFilename();
        String path;
        if ("img".equals(flag)) {
            path = PropertyUtil.prop.getProperty("path.img") + date.getTime() + fileName;
            String tmpPath = PropertyUtil.prop.getProperty("path.img");
            File newUploadFile = new File(tmpPath, date.getTime() + fileName);
            fi.transferTo(newUploadFile);
            mapReq.put(fi.getName(), path);
        } else if ("teamExam".equals(flag)) {
            System.out.println("开始");
            String name = date.getTime() + "";
            path = PropertyUtil.prop.getProperty("path.zip") + date.getTime() + fileName;
            String tmpPath = PropertyUtil.prop.getProperty("path.zip");
            File newUploadFile = new File(tmpPath, date.getTime() + fileName);
            fi.transferTo(newUploadFile);
            System.out.println("开始解压");
            String path2_1 = PropertyUtil.prop.getProperty("path.zip") + date.getTime() + "/test" + name;
            File path2_2 = new File(path2_1);
            if (!path2_2.exists() && !path2_2.isDirectory()) {
                System.out.println("//不存在");
                path2_2.mkdir();
            } else {
                System.out.println("//目录存在");
            }
            DeCompressUtil d = new DeCompressUtil();

            d.deCompress(path, path2_2.toString());
            // 删除zip
            DeleteFileUtil.deleteFile(path);
            // 扫描
            System.out.println("开始扫描");

            IODemo01 i = new IODemo01();
            File file_1 = new File(path2_2.toString());
            System.out.println("file_1 " + file_1);
            System.out.println("path2_2.toString() " + path2_2.toString());
            List<File> list = i.list(file_1);

            List<File> listTemp = new ArrayList<File>();
            Iterator<File> it = list.iterator();
            while (it.hasNext()) {
                File a = it.next();
                if (listTemp.contains(a)) {
                    it.remove();
                } else {
                    listTemp.add(a);
                }
            }
            System.out.println("开始压缩");
            path = PropertyUtil.prop.getProperty("path.teamexam") + name + "1.zip";

            File zipfile = new File(path);

            System.out.println("压缩到" + path);
            TestZIP.zipFiles1(list, zipfile);
            DeleteFileUtil.deleteDirectory(path2_1);
            mapReq.put(fi.getName(), path);
        } else {
            System.out.println("开始");
            String name = date.getTime() + "";
            path = PropertyUtil.prop.getProperty("path.zip") + date.getTime() + fileName;
            String tmpPath = PropertyUtil.prop.getProperty("path.zip");
            File newUploadFile = new File(tmpPath, date.getTime() + fileName);
            fi.transferTo(newUploadFile);
            System.out.println("开始解压");
            String path2_1 = PropertyUtil.prop.getProperty("path.zip") + date.getTime() + "/test" + name;
            File path2_2 = new File(path2_1);
            if (!path2_2.exists() && !path2_2.isDirectory()) {
                System.out.println("//不存在");
                path2_2.mkdir();
            } else {
                System.out.println("//目录存在");
            }
            DeCompressUtil d = new DeCompressUtil();

            d.deCompress(path, path2_2.toString());
            DeleteFileUtil.deleteFile(path);
            System.out.println("开始扫描");
            IODemo01 i = new IODemo01();
            File file_1 = new File(path2_2.toString());
            System.out.println("file_1 " + file_1);
            System.out.println("path2_2.toString() " + path2_2.toString());
            List<File> list = i.list(file_1);
            List<File> listTemp = new ArrayList<File>();
            Iterator<File> it = list.iterator();
            while (it.hasNext()) {
                File a = it.next();
                if (listTemp.contains(a)) {
                    it.remove();
                } else {
                    listTemp.add(a);
                }
            }
            System.out.println("开始压缩");
            path = PropertyUtil.prop.getProperty("path.selfexam") + name + "1.zip";

            File zipfile = new File(path);

            System.out.println("压缩到" + path);
            TestZIP.zipFiles1(list, zipfile);
            DeleteFileUtil.deleteDirectory(path2_1);
            mapReq.put(fi.getName(), path);
        }
        System.out.println("最终路径" + path);
        mapReq.put("filename", path.split("/")[path.split("/").length - 1]);
        return mapReq;
    }

}

