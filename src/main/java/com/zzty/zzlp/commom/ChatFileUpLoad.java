package com.zzty.zzlp.commom;



import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;

public class ChatFileUpLoad {
    public static Map<String, String> uploadFile(MultipartFile fi) throws Exception {
        Map<String, String> mapReq = new HashMap<String, String>();
        Date date = new Date();
        String fileName = fi.getOriginalFilename();
        String path;

        path = PropertyUtil.prop.getProperty("path.chat") + date.getTime() +"_"+ fileName;
        String tmpPath = PropertyUtil.prop.getProperty("path.chat");
        File newUploadFile = new File(tmpPath, date.getTime()  +"_"+ fileName);
        fi.transferTo(newUploadFile);
        mapReq.put(fi.getName(), path);


        System.out.println("最终路径" + path);
        mapReq.put("filename", date.getTime() +"_"+ fileName);
        return mapReq;
    }
}
