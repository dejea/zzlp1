package com.zzty.zzlp.commom;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class TimeGet {

    public static String nowTimeSteing(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String date = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
        return date;
    }
    public static Date nowTimeDate(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        java.util.Date time=null;
        try {
            time= sdf.parse(sdf.format(new Date()));

        } catch (ParseException e) {

            e.printStackTrace();
        }

        return time;
    }


}
