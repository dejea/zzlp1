package com.zzty.zzlp.commom;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class TestZIP {

	/**
	 * 功能:压缩多个文件成一个zip文件
	 *
	 * @param srcfile：源文件列表
	 * @param zipfile：压缩后的文件
	 */
	public static void zipFiles(File[] srcfile, File zipfile) {
		byte[] buf = new byte[1024];
		try {
			// ZipOutputStream类：完成文件或文件夹的压缩
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
			for (int i = 0; i < srcfile.length; i++) {
				FileInputStream in = new FileInputStream(srcfile[i]);
				out.putNextEntry(new ZipEntry(srcfile[i].getName()));
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.closeEntry();
				in.close();
			}
			out.close();
			System.out.println("压缩完成.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void zipFiles1(List<File> srcfile, File zipfile) {
		byte[] buf = new byte[1024];
		try {
			// ZipOutputStream类：完成文件或文件夹的压缩
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
			for (File attribute : srcfile) {
				FileInputStream in = new FileInputStream(attribute);

				out.putNextEntry(new ZipEntry(attribute.getName()));
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.closeEntry();
				in.close();
			}
			out.close();
			System.out.println("压缩完成.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sscZipFile(List<File> srcfile, File zipfile) throws Exception {
		String[] args = new String[srcfile.size()];
		int i = 0;
		for (File attribute : srcfile) {
			args[i] = attribute.getName();
			i++;

		}


		FileOutputStream f = new FileOutputStream(zipfile);
		CheckedOutputStream csum = new CheckedOutputStream(f, new Adler32());

		ZipOutputStream zos = new ZipOutputStream(csum);
		BufferedOutputStream out = new BufferedOutputStream(zos);
		zos.setComment("A test of Java Zipping");
		for (String arg : args) {
			System.out.println("writing file: " + arg);
			BufferedReader in = new BufferedReader(new FileReader(arg));
			zos.putNextEntry(new ZipEntry(arg));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			in.close();
			out.flush();
		}
		out.close();
		System.out.println("CheckSum: " + csum.getChecksum().getValue());
		System.out.println("Reading file");
		FileInputStream fi = new FileInputStream(zipfile);
		CheckedInputStream csumi = new CheckedInputStream(fi, new Adler32());
		ZipInputStream in2 = new ZipInputStream(csumi);
		BufferedInputStream bis = new BufferedInputStream(in2);
		ZipEntry ze;
		while ((ze = in2.getNextEntry()) != null) {
			System.out.println("Reading file " + ze);
			int x;
			while ((x = bis.read()) != -1) {
				System.out.write(x);
			}
		}
		if (args.length == 1)
			System.out.println("Checksum: " + csumi.getChecksum().getValue());
		bis.close();

		ZipFile zf = new ZipFile("zipfile");
		Enumeration e = zf.entries();
		while (e.hasMoreElements()) {
			ZipEntry ze2 = (ZipEntry) e.nextElement();
			System.out.println("File: " + ze2);
		}
	}

	/**
	 * 功能:解压缩
	 *
	 * @param zipfile：需要解压缩的文件
	 * @param descDir：解压后的目标目录
	 */
	public static void unZipFiles(File zipfile, String descDir) {
		try {
			ZipFile zf = new ZipFile(zipfile);
			for (Enumeration entries = zf.entries(); entries.hasMoreElements();) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				String zipEntryName = entry.getName();
				InputStream in = zf.getInputStream(entry);
				OutputStream out = new FileOutputStream(descDir + zipEntryName);
				byte[] buf1 = new byte[1024];
				int len;
				while ((len = in.read(buf1)) > 0) {
					out.write(buf1, 0, len);
				}
				in.close();
				out.close();
				System.out.println("解压缩完成.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 功能:
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * String a=""; //2个源文件 File f1 = new
		 * File("C:\\Users\\jiyu\\Desktop\\test\\1.txt"); File f2 = new
		 * File("C:\\Users\\jiyu\\Desktop\\test\\2.txt"); File[] srcfile = { f1,
		 * f2 }; List<File> list=new ArrayList<File>(); list.add(f1);
		 * list.add(f2); //System.out.println("begin");
		 *
		 * for(File attribute : list) {
		 *
		 * System.out.println(attribute.getName()); } //压缩后的文件 File zipfile =
		 * new File("C:\\Users\\jiyu\\Desktop\\3.zip"); TestZIP.zipFiles1(list,
		 * zipfile);
		 */

		/*
		 * //需要解压缩的文件 File file = new File("D:\\test\\3.zip"); //解压后的目标目录 String
		 * dir = "D:\\test\\1\\"; TestZIP.unZipFiles(file, dir);
		 */
	}
}