package com.zzty.zzlp.commom;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//扫描目录
public class IODemo01 {
    static List<File> list;

    /*
     * public static void main(String[] args) { File file = new
     * File("C:\\Users\\jiyu\\Desktop\\test"); list(file); }
     */
    public IODemo01() {
        list = new ArrayList<File>();

    }

    public static List<File> list(File file) {
        if (file.isDirectory())// 判断file是否是目录
        {
            File[] lists = file.listFiles();
            if (lists != null) {
                for (int i = 0; i < lists.length; i++) {
                    list(lists[i]);// 是目录就递归进入目录内再进行判断
                }
            }
        }
        String type = file.toString();
        String name = type.substring(type.lastIndexOf(".") + 1);

        if (name.equals("java") || name.equals("js") || name.equals("jsp") || name.equals("html")) {
            // System.out.println(type);// file不是目录，就输出它的路径名，这是递归的出口
            list.add(file);
            // System.out.println(list.size());// file不是目录，就输出它的路径名，这是递归的出口

        }
        return list;
    }
}