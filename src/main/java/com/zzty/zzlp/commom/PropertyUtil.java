package com.zzty.zzlp.commom;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {
	public static Properties prop = new Properties(); 
	static{
		 InputStream in = PropertyUtil.class.getResourceAsStream("/application.properties");   
	        try {   
	            prop.load(in);   
	        } catch (IOException e) {   
	            e.printStackTrace();   
	        }   
	}

}