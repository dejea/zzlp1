package com.zzty.zzlp.serviceimpl;

import com.zzty.zzlp.dao.UserMapper;
import com.zzty.zzlp.dao.VtmassageMapper;
import com.zzty.zzlp.entity.User;
import com.zzty.zzlp.entity.Vtmassage;
import com.zzty.zzlp.service.UserService;
import com.zzty.zzlp.service.VtmassageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    UserMapper um;
    @Override
    public User searchUser(User user) {
        return um.searchUser(user);
    }
}
