package com.zzty.zzlp.serviceimpl;

import com.zzty.zzlp.dao.VtmassageMapper;
import com.zzty.zzlp.entity.Vtmassage;
import com.zzty.zzlp.service.VtmassageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VtmassageServiceImpl implements VtmassageService {


    @Autowired
    VtmassageMapper vm;


    @Override
    public Boolean addVtmassages(Vtmassage vtmassages) {

        vm.addVtmassages(vtmassages);

        return true;
    }

    @Override
    public List<Vtmassage> searchVtmassages() {
        return vm.searchVtmassages();
    }

    @Override
    public void deleteCus() {
        vm.deleteCus();
    }
}
